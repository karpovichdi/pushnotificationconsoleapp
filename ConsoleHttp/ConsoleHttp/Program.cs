﻿using Microsoft.Azure.NotificationHubs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleHttp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tap enter for push notification");
            while(Console.ReadKey().Key == ConsoleKey.Enter)
            {
                SendTemplateNotificationsAsync().GetAwaiter().GetResult();
            }
        }
        
        private static async Task SendTemplateNotificationsAsync()
        {
            var hub = NotificationHubClient.CreateClientFromConnectionString(DispatcherConstants.FullAccessConnectionString, DispatcherConstants.NotificationHubName);
            
            try
            {
                await hub.SendFcmNativeNotificationAsync(DispatcherConstants.AndroidMessageExample, DispatcherConstants.SubscriptionTags);
                Console.WriteLine("Notification successfully sent");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Sent messages to {DispatcherConstants.SubscriptionTags.Length} tags.");
            }
        }
    }
}